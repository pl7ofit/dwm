/* list keyboard layouts /usr/share/X11/xkb/rules/base.lst */
static const char *const autostart[] = {
       "xinput", "--set-prop", mouse_id, "libinput Accel Speed", mouse_accel, NULL,
       "xset", "r", "rate", kb_delay, kb_repeat, NULL,
       "dwm-bg.sh", NULL,
       "dwm-bar.sh", NULL,
       "dwm-layout.sh", NULL,
       "nm-applet", NULL,
       "mate-polkit", NULL,
       "pulseaudio", NULL,
       "/home/pl7ofit/.screenlayout/default.sh", NULL,
       "picom", NULL,
       NULL /* terminate */
};

