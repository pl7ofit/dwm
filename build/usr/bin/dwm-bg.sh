#!/bin/sh


bg_file="/home/pl7ofit/.wallpaper"
while [ ! -e "${bg_file}" ]; do sleep 1;done
feh --bg-scale ${bg_file}
bg_file_hash="$(stat -L --format '%i%Z%Y%X%s%b' ${bg_file})"

while true
do
  if [ -e "${bg_file}" ] && [ "$(stat -L --format '%i%Z%Y%X%s%b' ${bg_file})" != "${bg_file_hash}" ]
  then
    feh --bg-scale ${bg_file}
    bg_file_hash="$(stat -L --format '%i%Z%Y%X%s%b' ${bg_file})"
  fi
  sleep 1
done
