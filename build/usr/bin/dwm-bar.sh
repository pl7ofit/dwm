#!/bin/sh

set -eu

while :
do
  sp_volume_data="$(amixer -n -a none -M -D pulse get Master | grep -Po -m1 '([0-9]{1,3})(?:....)(on|off)' | tr -d '[]%')"
  sp_volume="$(printf "$sp_volume_data" | cut -d' ' -f1 )"
  sp_active="$(printf "$sp_volume_data" | cut -d' ' -f2 )"
  mc_volume_data="$(amixer -n -a none -M -D pulse get Capture | grep -Po -m1 '([0-9]{1,3})(?:....)(on|off)' | tr -d '[]%')"
  mc_volume="$(printf "$mc_volume_data" | cut -d' ' -f1 )"
  mc_active="$(printf "$mc_volume_data" | cut -d' ' -f2 )"
  br_data=""
  #"$(xrandr --verbose | grep 'Brightness' | cut -d ':' -f2)"

  dtime=$(date "+%a %y.%m.%d %H:%M:%S")
  layout="$(xset -q | grep -Po "[0-9]{8}" | sed 's|0000000.|en|g' | sed 's|0000100.|ru|g')"

  if [ "$sp_active" = "off"  ]
  then
    sp_sym="🔇"
  else
    sp_sym="🔊"
  fi

  if [ "$mc_active" = "off"  ]
  then
    mc_sym="🔇"
  else
    mc_sym="🎤"
  fi

  br_sym="💡"

  string="${sp_sym}$(printf '% 4d' ${sp_volume})% ⋅ ${mc_sym}$(printf '% 4d' ${mc_volume})% ⋅ [${layout}] ⋅ ${dtime} ⋅"
  xsetroot -name "$string"
  sleep 0.3
done
