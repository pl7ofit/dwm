#!/bin/sh

layouts_file=/home/pl7ofit/.layouts
while [ -f ${layouts_file} ]
do
  layouts="$(sed -n '1p' ${layouts_file} | tr -d '\n')"
  layouts_switch="$(sed -n '2p' ${layouts_file} | tr -d '\n')"
  if [ $(setxkbmap -query | grep -c "${layouts}") = 0 ] || [ $(setxkbmap -query | grep -c "${layouts_switch}") = 0 ]
  then
    setxkbmap -query
    setxkbmap -option
    setxkbmap -layout  "${layouts}" -option  "${layouts_switch}"
  fi
  sleep 1
done

exit 1
