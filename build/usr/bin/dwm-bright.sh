#!/bin/sh

current_brightness="$(xrandr --verbose | grep 'Brightness' | cut -d ':' -f2)"
step=0.1
min=0.2
max=2.0
conn="HDMI-1"

if [ "$1" = 'set' ]
then
  if [ "$2" = 'up' ]
  then
    val="$(echo "${current_brightness}+${step}" | bc -l)"
    if [ "$(echo "${val}<=${max}" | bc -l)" = '1' ]
    then
      echo "${val}"
      xrandr --output ${conn} --brightness "$(echo ${current_brightness}+${step} | bc -l)"
    fi
  elif [ "$2" = 'down' ]
  then
    val="$(echo ${current_brightness}-${step} | bc -l)"
    if [ "$(echo "${val}>=${min}" | bc -l)" = '1' ]
    then
      echo "${val}"
      xrandr --output ${conn} --brightness "$(echo ${current_brightness}-${step} | bc -l)"
    fi
  elif [ "$2" = 'def' ]
  then
    echo 1.0
    xrandr --output ${conn} --brightness 1.0
  else
    exit 1
  fi

elif [ "$1" = 'get' ]
then
  echo ${current_brightness}
else
  exit 1
fi
