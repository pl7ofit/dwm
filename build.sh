#!/bin/bash

set -eu

name=dwm
version="6.2-git"
repo="git://git.suckless.org/dwm"
mouse_id="$(DISPLAY=:0 xinput | grep 'Mouse' | awk -F'\t' '{print $2}' | sed 's/id=//g'| sort -V| head -n1)"

echo $mouse_id
docker build -f Dockerfile . -t dwm:latest \
       --build-arg "version=$version" \
       --build-arg "name=$name" \
       --build-arg "repo=$repo" \
       --build-arg "mouse_id=$mouse_id" 
original_hash="$(docker run  dwm:latest sh -c "sha256sum dwm-$version.deb")"
docker run dwm:latest  sh -c "cat dwm-$version.deb" > dwm-$version.deb
copy_hash="$(sha256sum dwm-$version.deb)"
echo $original_hash
echo $copy_hash
