/* key definitions */

#define TAGKEYS(KEY,TAG) \
        { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
        { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
        { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
        { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "x-terminal-emulator", NULL };
static const char *killwm[]  = { "killall", "dwm", NULL };


/* Volume control */


static const char *spvolup[] = {"amixer", "-D", sound_driver, "set", sp_name, "5%+", "unmute", NULL};
static const char *spvoldown[] = {"amixer", "-D", sound_driver, "set", sp_name, "5%-", NULL};
static const char *spvolmute[] = {"amixer", "-D", sound_driver, "set", sp_name, "1+", "toggle", NULL};

static const char *mcvolup[] = {"amixer", "-D", sound_driver, "set", mc_name, "5%+", "unmute", NULL};
static const char *mcvoldown[] = {"amixer", "-D", sound_driver, "set", mc_name, "5%-", NULL};
static const char *mcvolmute[] = {"amixer", "-D", sound_driver, "set", mc_name, "1+", "toggle", NULL};

/* Brightness control */
static const char *dsbrup[] = {"dwm-bright.sh","set","up", NULL};
static const char *dsbrdown[] = {"dwm-bright.sh","set","down", NULL};
static const char *dsbrdef[] = {"dwm-bright.sh","set","def", NULL};


/* Screenshot */

static const char *screenshotfull[] = {"flameshot", "full", "-p", screenshot_path,  NULL};
static const char *screenshotarea[] = {"flameshot", "gui", "-p", screenshot_path,  NULL};

/* Screenlock */
static const char *screenlock[] = {"sh", "-c",
	                           "XSECURELOCK_PASSWORD_PROMPT=asterisks "
	                           "XSECURELOCK_NO_COMPOSITE=1 "
				   "XSECURELOCK_DISCARD_FIRST_KEYPRESS=1 "
				   "XSECURELOCK_COMPOSITE_OBSCURER=1 "
				   "XSECURELOCK_BLANK_DPMS_STATE=suspend "
				   "XSECURELOCK_BLANK_TIMEOUT=5 "
				   "XSECURELOCK_AUTH_BACKGROUND_COLOR='#110000' "
				   "XSECURELOCK_AUTH_FOREGROUND_COLOR='#00cc00' "
				   "XSECURELOCK_AUTH_SOUNDS=1 "
				   "XSECURELOCK_BLANK_TIMEOUT=5 "
				   "XSECURELOCK_AUTH_TIMEOUT=5 "
	                           "xsecurelock", 
				   NULL};
static Key keys[] = {
        /* modifier                     key                function           argument */
        // Menu
        { MODKEY,                       XK_r,              spawn,             {.v = dmenucmd } },
        // Terminal
        { MODKEY|ControlMask,           XK_t,              spawn,             {.v = termcmd } },
        // Hide bar
        { MODKEY,                       XK_b,              togglebar,         {0} },
        // Switch window
        { MODKEY,                       XK_grave,          focusstack,        {.i = +1 } },
        { MODKEY,                       XK_Tab,            focusstack,        {.i = -1 } },
        // Move master window
        { MODKEY,                       XK_w,              incnmaster,        {.i = +1 } },
        { MODKEY,                       XK_s,              incnmaster,        {.i = -1 } },
        // Resize window horizontal
        { MODKEY,                       XK_a,              setmfact,          {.f = -0.05} },
        { MODKEY,                       XK_d,              setmfact,          {.f = +0.05} },
        // Select focused window as master
        { MODKEY,                       XK_f,              zoom,              {0} },
        //
        { MODKEY,                       XK_z,              view,              {0} },
        // Close window
        { MODKEY,                       XK_c,              killclient,        {0} },
        // Select layout
        { MODKEY|ShiftMask,             XK_t,              setlayout,         {.v = &layouts[0]} },
        { MODKEY|ShiftMask,             XK_f,              setlayout,         {.v = &layouts[1]} },
        { MODKEY|ShiftMask,             XK_m,              setlayout,         {.v = &layouts[2]} },
	// Switch layouts
        { MODKEY,                       XK_space,          setlayout,         {0} },
        { MODKEY|ControlMask,           XK_space,          togglefloating,    {0} },
        { MODKEY,                       XK_0,              view,              {.ui = ~0 } },
        { MODKEY|ControlMask|ShiftMask, XK_0,              tag,               {.ui = ~0 } },

        // MONs
        { MODKEY,                       XK_bracketleft,    focusmon,          {.i = -1 } },
        { MODKEY,                       XK_bracketright,   focusmon,          {.i = +1 } },
        { MODKEY|ControlMask,           XK_bracketleft,    tagmon,            {.i = -1 } },
        { MODKEY|ControlMask,           XK_bracketright,   tagmon,            {.i = +1 } },
        // TAGS
         TAGKEYS(                       XK_1,0)
         TAGKEYS(                       XK_2,1)
         TAGKEYS(                       XK_3,2)
         TAGKEYS(                       XK_4,3)
         TAGKEYS(                       XK_5,4)
         TAGKEYS(                       XK_6,5)
         TAGKEYS(                       XK_7,6)
        // QUIT
        { MODKEY|ControlMask|ShiftMask, XK_r,              quit,              {0} },
        { MODKEY|ControlMask|ShiftMask, XK_q,              spawn,             {.v = killwm } },
        // Volume speaker
        { MODKEY|ShiftMask,             XK_equal,          spawn,             {.v = spvolup } },
        { MODKEY|ShiftMask,             XK_minus,          spawn,             {.v = spvoldown } },
        { MODKEY|ShiftMask,             XK_0,              spawn,             {.v = spvolmute } },
        // Volume microphone
        { MODKEY|ControlMask,           XK_equal,          spawn,             {.v = mcvolup } },
        { MODKEY|ControlMask,           XK_minus,          spawn,             {.v = mcvoldown } },
        { MODKEY|ControlMask,           XK_0,              spawn,             {.v = mcvolmute } },
	//Brightness
        { MODKEY|ControlMask|ShiftMask, XK_equal,          spawn,             {.v = dsbrup } },
        { MODKEY|ControlMask|ShiftMask, XK_minus,          spawn,             {.v = dsbrdown } },
        { MODKEY|ControlMask|ShiftMask, XK_0,              spawn,             {.v = dsbrdef } },
        // Screenshot
        { 0,                            XK_Print,          spawn,             {.v = screenshotfull } },
        { ControlMask,                  XK_Print,          spawn,             {.v = screenshotarea } },
	// Screenlock
	{ MODKEY|ControlMask|ShiftMask, XK_l,              spawn,             {.v = screenlock} },

};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
        /* click                event mask                       button          function        argument */
        { ClkLtSymbol,          0,                               Button1,        setlayout,      {0} },
        { ClkLtSymbol,          0,                               Button3,        setlayout,      {.v = &layouts[2]} },
        { ClkWinTitle,          0,                               Button2,        zoom,           {0} },

	{ ClkStatusText,        MODKEY|ShiftMask,                Button4,        spawn,          {.v = spvolup } },
        { ClkStatusText,        MODKEY|ShiftMask,                Button5,        spawn,          {.v = spvoldown } },
        { ClkStatusText,        MODKEY|ShiftMask,                Button1,        spawn,          {.v = spvolmute } },

        { ClkStatusText,        MODKEY|ControlMask,              Button4,        spawn,          {.v = mcvolup } },
        { ClkStatusText,        MODKEY|ControlMask,              Button5,        spawn,          {.v = mcvoldown } },
        { ClkStatusText,        MODKEY|ControlMask,              Button1,        spawn,          {.v = mcvolmute } },

        { ClkStatusText,        MODKEY|ControlMask|ShiftMask,    Button4,        spawn,          {.v = dsbrup } },
        { ClkStatusText,        MODKEY|ControlMask|ShiftMask,    Button5,        spawn,          {.v = dsbrdown } },
        { ClkStatusText,        MODKEY|ControlMask|ShiftMask,    Button1,        spawn,          {.v = dsbrdef } },

        { ClkClientWin,         MODKEY,                          Button1,        movemouse,      {0} },
        { ClkClientWin,         MODKEY,                          Button2,        togglefloating, {0} },
        { ClkClientWin,         MODKEY,                          Button3,        resizemouse,    {0} },
        { ClkTagBar,            0,                               Button1,        view,           {0} },
        { ClkTagBar,            0,                               Button3,        toggleview,     {0} },
        { ClkTagBar,            MODKEY,                          Button1,        tag,            {0} },
        { ClkTagBar,            MODKEY,                          Button3,        toggletag,      {0} },
};
