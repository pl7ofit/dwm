/* See LICENSE file for copyright and license details. */

// Mouse
#define mouse_id "9"
#define mouse_accel "-0.8"


// Keyboard
#define kb_delay "300"
#define kb_repeat "50"

// Modkeys
#define MODKEY Mod1Mask //Alt

// Sound
#define sp_name "Master"
#define mc_name "Capture"
#define sound_driver "pulse"

// Screenshots
#define screenshot_path "/home/pl7ofit/Pictures/Screenshots/"

/* appearance */
static const unsigned int borderpx       = 2;        /* border pixel of windows */
static const unsigned int gappx          = 8;        /* gap pixel between windows */
static const unsigned int snap           = 32;       /* snap pixel */


/* tray */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 0;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;   /* 0 means no systray */

/* bar */
static const int showbar                 = 1;        /* 0 means no bar */
static const int topbar                  = 1;        /* 0 means bottom bar */
static const int user_bh                 = 20;       /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]             = {
					 "DejaVu Sans Condensed:Bold:size=10:antialias=true:autohint=trueAe",
                                         "Symbola:Bold:size=10:antialias=true:autohint=trueAe"
                                         };
/* menu */
static const char dmenufont[]            = "monospace:size=10";
/* colors */
static const char col_black[]            = "#000000";
static const char col_white[]            = "#ffffff";
static const char col_gray[]             = "#141414";
static const char col_gray1[]            = "#222222";
static const char col_gray2[]            = "#444444";
static const char col_gray3[]            = "#bbbbbb";
static const char col_gray4[]            = "#eeeeee";
static const char col_cyan[]             = "#002545";
static const char col_cyan1[]            = "#004060";
//static const char col_cyan2_1[]          = "#23626a";
static const char col_cyan2[]            = "#09404a";
static const char col_cyan2_1[]          = "#00a6a1";
static const char col_cyan3[]            = "#00fff8";
static const char col_red[]              = "#400000";
static const char col_red1[]             = "#900000";
static const char col_red2[]             = "#ff0000";
static const char col_violet1[]          = "#171419";
static const char col_violet2[]          = "#322d38";
static const char col_violet3[]          = "#5e5367";
static const char col_violet5[]          = "#ded1e8";
static const char col_yellow3[]          = "#eac9bb";
static const char col_yellow5[]          = "#eacbac";
static const char col_orange[]          = "#5d2b00";
static const char col_orange1[]          = "#9c4e00";
static const char col_orange3[]          = "#eaa182";

static const char *colors[][3]           = {
               	                          /*               fg         bg         border   */
                                      	[SchemeNorm] = { col_yellow5, col_gray, col_black },
                                	[SchemeSel]  = { col_yellow5, col_violet2,  col_orange1 },
                                        };




/* autostart */
#include "autostart.h"

/* tagging */
static const char *tags[] = { "🌍₁","🔨₂","💬₃","🎮₄","📻₅","📧₆","📌₇" };

#include "rules.h"

/* layout(s) */
static const float mfact                 = 0.50; /* factor of master area size [0.05..0.95] */
static const int   nmaster               = 1;    /* number of clients in master area */
static const int resizehints             = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen          = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[H]",      tile },    /* first entry is default */
	{ "[~]",      NULL },    /* no layout function means floating behavior */
	{ "[ ]",      monocle },
};

/* key definitions */
#include "keybinds.h"
